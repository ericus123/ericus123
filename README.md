### Hi there, I'm AMANI Eric

## A full stack Software Developer

- 🌱 I’m currently learning **Three.js**
- 👯 I’m looking forward to collaborating with other programmers
- 🥅 2022 Goals: Learn 3D development (web && gaming)
- ⚡ Fun fact: I'm a Hardware Programmer 🤖 (hobbyst), Cyber Security 🛡️ enthusiast and I'm good at soccer ⚽

### Connect with me:

[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://www.amanieric.com/)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/amani-eric/)
[![twitter](https://img.shields.io/badge/twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/amaniericus)

### Languages and Tools:

<img align="left" alt="Visual Studio Code" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png" />
<img align="left" alt="HTML5" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/html/html.png" />
<img align="left" alt="CSS3" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/css/css.png" />
<img align="left" alt="Sass" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/sass/sass.png" />
<img align="left" alt="JavaScript" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png" />
<img align="left" alt="React" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/react/react.png" />
<img align="left" alt="Gatsby" width="26px" src="https://raw.githubusercontent.com/github/explore/e94815998e4e0713912fed477a1f346ec04c3da2/topics/gatsby/gatsby.png" />
<img align="left" alt="Node.js" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/nodejs/nodejs.png" />
<img align="left" alt="SQL" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/sql/sql.png" />

<img align="left" alt="MongoDB" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/mongodb/mongodb.png" />
<img align="left" alt="MongoDB" width="26px" src="https://brandslogos.com/wp-content/uploads/images/large/arduino-logo-1.png" />
<img align="left" alt="Git" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/git/git.png" />
<img align="left" alt="GitHub" width="26px" src="https://raw.githubusercontent.com/github/explore/78df643247d429f6cc873026c0622819ad797942/topics/github/github.png" />
<img align="left" alt="Terminal" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/terminal/terminal.png" />
<img align="left" alt="Heroku" width="26px" src="https://cdn.icon-icons.com/icons2/2108/PNG/512/heroku_icon_130912.png" />
<img align="left" alt="Figma" width="26px" src="https://www.seekicon.com/free-icon-download/figma-icon_5.svg" />
<img align="left" alt="Travis" width="26px" src="https://www.seekicon.com/free-icon-download/travis-ci-icon_1.svg" />
<img align="left" alt="Netlify" width="26px" src="https://www.seekicon.com/free-icon-download/netlify-icon_2.svg" />
<img align="left" alt="Netlify" width="26px" src="https://pics.freeicons.io/uploads/icons/png/9114856761551941711-512.png" />
<img align="left" alt="NextJS" width="26px" src="https://pics.freeicons.io/uploads/icons/png/14678610731551953708-512.png" />
<img align="left" alt="PostgreSQL" width="26px" src="https://pics.freeicons.io/uploads/icons/png/16876668881551942134-512.png" />
<img align="left" alt="Firebase" width="26px" src="https://pics.freeicons.io/uploads/icons/png/6247864081536298180-512.png" />
<br />
<br />

---
